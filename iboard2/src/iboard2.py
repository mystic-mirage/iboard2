#!/usr/bin/python
# - * - coding: utf-8 - * -

'''
@author: Aleksandr Tishin
@contact: aleksandr.tishin@gmail.com
'''

import pyglet
import time
import string
import urllib
import BaseHTTPServer
import threading
import cgi
import os
import datetime
import sys
import xlrd
import pickle
import pymouse

lock = threading.Lock()

changed = True
updated = False
clocks = True
layout = []
wlist = [0, 0, 0, 0, 0, 0, 0]

def readSettings():
    global ovalues
    f = open('settings')
    svalues = f.readlines()
    f.close()
    ovalues = []
    for i in svalues:
        ovalues.append(string.strip(string.split(i, '#')[0]))

def readJudges():
    f = open('judges')
    svalues = f.readlines()
    f.close()
    jvalues = []
    for i in svalues:
        jvalues.append(string.split(string.strip(i), '*'))
    return jvalues

def readRooms():
    f = open('rooms')
    svalues = f.readlines()
    f.close()
    return map(string.strip, svalues)

class RowItem():
    def __init__(self, row, Upper):
        self.cells = []
        self.Upper = Upper
        self.row = row
        self.position = -1
        self.height = 0
    def __del__(self):
        pass
    def getUpper(self):
        return self.Upper
    def moveUp(self):
        if self.Upper:
            self.Upper.moveUp()
            if self.Upper.canFollow():
                self.position = self.position + 1
        else:
            self.position = self.position + 1
        if self.position > -1 and self.position <= (workh + self.height - layhead[0].height) and self.cells == []:
            for cel in range(len(self.row) - 2):
                document = pyglet.text.decode_attributed(("""%s%s%s""" % (ovalues[1], ovalues[cel + 2], self.row[cel].decode('utf-8'))) % (workh / int(ovalues[16])))
                layout = pyglet.text.layout.IncrementalTextLayout(document, workw, workh)
                layout.height = layout.content_height
                #layout.width = layout.content_width
                if (wlist[cel] - workw / 64) > layout.content_width:
                    layout.width = wlist[cel]
                else:
                    wlist[cel] = layout.width = layout.content_width + workw / 64
                layout.anchor_y = 'top'
                layout.x = sum(wlist[:cel]) + workw / 64 + window.width / int(ovalues[9])
                self.cells.append(layout)
            document = pyglet.text.decode_attributed(("""%s%s%s""" % (ovalues[1], ovalues[7], self.row[5].decode('utf-8').replace('\r\n', os.linesep).replace('-', '- '))) % (workh / int(ovalues[16])))
            wlist[5] = (workw - sum(wlist[:5])) / 2
            layout = pyglet.text.layout.IncrementalTextLayout(document, wlist[5] - workw / 64, workh,
                                                              multiline=True)
            layout.height = layout.content_height
            layout.anchor_y = 'top'
            layout.x = sum(wlist[:5]) + workw / 64 + window.width / int(ovalues[9])
            self.cells.append(layout)
            document = pyglet.text.decode_attributed(("""%s%s%s""" % (ovalues[1], ovalues[8], self.row[6].decode('utf-8').replace('\r\n', os.linesep).replace('-', '- '))) % (workh / int(ovalues[16])))
            wlist[6] = workw - sum(wlist[:6]) - workw / 64
            layout = pyglet.text.layout.IncrementalTextLayout(document, wlist[6], workh,
                                                              multiline=True)
            layout.height = layout.content_height
            layout.anchor_y = 'top'
            layout.x = sum(wlist[:6]) + workw / 64 + window.width / int(ovalues[9])
            self.cells.append(layout)
            self.height = max([cel.height for cel in self.cells]) + (workh / 64)
        if self.position > (workh + self.height - layhead[0].height) and self.cells != []:
            del self.cells
            self.cells = []
    def canFollow(self):
        return (self.position > self.height) 
    def onceAgain(self):
        if self.Upper:
            self.Upper.onceAgain()
        self.position = -1
        for c in self.cells:
            c.y = -1
    def draw(self):
        if self.Upper:
            self.Upper.draw()
        if self.cells != []:
            for cell in self.cells:
                cell.y = self.position
                cell.draw()

def updateRows():
    global changed
    global updated
    global upper
    global clocks
    jvalues = readJudges()
    jlnum = 0
    upper = None
    for i in [a[0] for a in jvalues]:
        lock.acquire()
        try:
            try:
                f = open('caselist/%s.list' % i)
            except IOError:
                pass
            else:
                jlnum = jlnum + 1
                caselist = pickle.Unpickler(f).load()
                f.close()
                for r in caselist:
                    upper = RowItem(r, upper)
        finally:
            lock.release()
    clocks = not bool(jlnum)
    changed = updated = False

platform = pyglet.window.get_platform()
display = platform.get_default_display()
screen = display.get_screens()
window = pyglet.window.Window(screen = screen[-1], fullscreen = True)
#window = pyglet.window.Window(1280, 720)
window.set_mouse_visible(False)

@window.event
def on_draw():
    window.clear()
    if not clocks:
        upper.draw()
        pyglet.graphics.draw(4, pyglet.gl.GL_QUADS,
                             ('v2f', (0, window.height - layhead[0].height - window.height / int(ovalues[11]),
                                      0, window.height,
                                      window.width, window.height,
                                      window.width, window.height - layhead[0].height - window.height / int(ovalues[11]))),
                                      ('c3B', (0, 0, 0) * 4))
        for cel in range(len(layhead)):
            layhead[cel].draw()
    else:
        image.blit(window.width / 2, window.height / 2)

pause = 0

def callback(dt):
    global pause
    global clocks
    if changed:
        updateRows()
    if not clocks:
        upper.moveUp()
        for cel in range(len(layhead)):
            layhead[cel].x = sum(wlist[:cel]) + window.width / int(ovalues[9])# + workw / 64
            layhead[cel].width = wlist[cel]
        if upper.position > (workh + upper.height):
            upper.onceAgain()
            pause = int(ovalues[12])
            clocks = True

mouse = pymouse.PyMouse()

def clockback(dt):
    global clocks
    global pause
    if pause:
        pause = pause - 1
        if not pause:
            clocks = False
    mouse.move(mouse.position()[0], mouse.position()[1])

def getproptime(book, cell):
    if book.format_map[book.xf_list[cell.xf_index].format_key].format_str == u'GENERAL':
        return unicode(cell.value.strip())
    else:
        try:
            mins = int(cell.value * 1440)
            return unicode('%d:%02d' % (mins / 60, mins % 60))
        except:
            return unicode(cell.value.strip())

def getpropnum(book, cell):
    return unicode(cell.value).replace('.0', '').strip()

readSettings()

class WIHandler(BaseHTTPServer.BaseHTTPRequestHandler):
    def do_POST(self):
        """Respond to a POST request."""
        global changed
        global clocks
        form = cgi.FieldStorage(
            fp=self.rfile, 
            headers=self.headers,
            environ={'REQUEST_METHOD':'POST',
                     'CONTENT_TYPE':self.headers['Content-Type'],
                     })
        jvalues = readJudges()
        path = map(urllib.unquote, string.split(self.path, '/'))
        if path[1]:
            if path[1] == 'administrator':
                pass
            elif path[1] in [a[0] for a in jvalues] and path[2]:
                if form['casefile'].filename:
                    xlsfilename = '%s_%s_%s_%s' % (datetime.date.today(), path[1], datetime.datetime.now().strftime('%H-%M-%S'), os.path.basename(form['casefile'].filename))
                    print '*' * 32
                    print '+File uploaded+'
                    print 'Judge: %s' % path[1]
                    print 'Room: %s' % form['room'].value
                    print 'File: %s' % xlsfilename
                    print 'Client: %s' % self.client_address[0]
                    print '*' * 32
                    f = open('temp/%s' % xlsfilename, 'wb')
                    f.write(form['casefile'].file.read())
                    f.close()
                    caselist = []
                    try:
                        wb = xlrd.open_workbook('temp/%s' % xlsfilename, formatting_info = True)
                    except:
                        print '*' * 32
                        print '-Wrong file-'
                        print 'Judge: %s' % path[1]
                        print 'Room: %s' % form['room'].value
                        print 'File: %s' % xlsfilename
                        print 'Client: %s' % self.client_address[0]
                        print '*' * 32
                        self.send_response(200)
                        self.send_header("Content-type", "text/html")
                        self.end_headers()
                        top = """<html><head><meta http-equiv="content-type" content="text/html; charset=UTF-8"><title>%s</title></head><body><center><style type="text/css"><!-- a {color: blue; text-decoration: none;} a:hover {color: green;} a.a {color: red;} --></style>"""
                        bottom = """</center></body></html>"""
                        self.wfile.write(top % path[1])
                        self.wfile.write("""<h1>Неправильний файл!</h1>""")
                        self.wfile.write("""<br /><a href="/%s/">Спробувати знову</a><br />""" % path[1])
                        self.wfile.write("""<br /><a href="/">Повернутися до вибору суддів</a>""")
                        self.wfile.write(bottom)
                    else:
                        sh = wb.sheet_by_index(0)
                        for rownum in range(sh.nrows - 8):
                            if getpropnum(wb, sh.cell(rownum + 8, 0)):
                                caselist.append([getpropnum(wb, sh.cell(rownum + 8, 0)), path[1], form['room'].value,
                                                 getproptime(wb, sh.cell(rownum + 8, 1))]
                                                 + map(lambda x: unicode(x).encode('utf-8'), sh.row_values(rownum + 8)[2:5]))
                        rvalues = readRooms()
                        self.send_response(200)
                        self.send_header("Content-type", "text/html")
                        self.end_headers()
                        top = """<html><head><meta http-equiv="content-type" content="text/html; charset=UTF-8"><title>%s</title></head><body><center><style type="text/css"><!-- a {color: blue; text-decoration: none;} a:hover {color: green;} a.a {color: red;} --></style>"""
                        bottom = """</center></body></html>"""
                        self.wfile.write(top % path[1])
                        self.wfile.write("""<form action="/%s/" method="post"><table border="1" cellpadding="4">""" % path[1])
                        self.wfile.write("""<thead><tr><td>№ п/п</td><td>Суддя</td><td>№ залу</td><td>Час слухання</td><td>№ справи</td><td>Позивач</td><td>Відповідач</td></tr></thead>""")
                        for r in range(len(caselist)):
                            self.wfile.write("""<tbody><tr>""")
                            self.wfile.write("""<td><input name="casefield%s0" size="2" value="%s" /></td>""" % (r, caselist[r][0]))
                            self.wfile.write("""<td>%s</td>""" % caselist[r][1])
                            self.wfile.write("""<td><select name="caseroom%s">""" % r)
                            for i in rvalues:
                                self.wfile.write("""<option%s>%s""" % (' selected' * (caselist[r][2] == i), i))
                            self.wfile.write("""</td>""")
                            self.wfile.write("""<td><input name="casefield%s1" size="4" value="%s" /></td>""" % (r, caselist[r][3]))
                            self.wfile.write("""<td><input name="casefield%s2" size="16" value="%s" /></td>""" % (r, caselist[r][4]))
                            self.wfile.write("""<td><textarea name="casefield%s3" rows="4" cols="32">%s</textarea></td>""" % (r, caselist[r][5]))
                            self.wfile.write("""<td><textarea name="casefield%s4" rows="4" cols="32">%s</textarea></td>""" % (r, caselist[r][6]))
                            self.wfile.write("""</tr></tbody>""")
                        self.wfile.write("""</table><br /><button type="reset">Скасувати зміни</button>&nbsp;""")
                        self.wfile.write("""<button type="submit">Зберегти</button></form>""")
                        self.wfile.write("""<br /><a href="/%s/">Спробувати знову</a><br />""" % path[1])
                        self.wfile.write("""<br /><a href="/">Повернутися до вибору суддів</a>""")
                        self.wfile.write(bottom)
                        print '*' * 32
                        print '+Parsing success+'
                        print 'Judge: %s' % path[1]
                        print 'Room: %s' % form['room'].value
                        print 'File: %s' % xlsfilename
                        print 'Client: %s' % self.client_address[0]
                        print '*' * 32
                else:
                    print '*' * 32
                    print '-No file-'
                    print 'Judge: %s' % path[1]
                    print 'Room: %s' % form['room'].value
                    print 'Client: %s' % self.client_address[0]
                    print '*' * 32
                    self.send_response(200)
                    self.send_header("Content-type", "text/html")
                    self.end_headers()
                    top = """<html><head><meta http-equiv="content-type" content="text/html; charset=UTF-8"><title>%s</title></head><body><center><style type="text/css"><!-- a {color: blue; text-decoration: none;} a:hover {color: green;} a.a {color: red;} --></style>"""
                    bottom = """</center></body></html>"""
                    self.wfile.write(top % path[1])
                    self.wfile.write("""<h1>Файл не вказаний!</h1>""")
                    self.wfile.write("""<br /><a href="/%s/">Спробувати знову</a><br />""" % path[1])
                    self.wfile.write("""<br /><a href="/">Повернутися до вибору суддів</a>""")
                    self.wfile.write(bottom)
            elif path[1] in [a[0] for a in jvalues] and not path[2]:
                caselist = []
                r = 0
                while True:
                    try:
                        form['caseroom%s' % r].value
                    except:
                        break
                    caselist.append([form['casefield%s0' % r].value.strip(), path[1], form['caseroom%s' % r].value,
                                    form['casefield%s1' % r].value.strip(), form['casefield%s2' % r].value.strip(),
                                    form['casefield%s3' % r].value.strip(), form['casefield%s4' % r].value.strip()])
                    r = r + 1
                lock.acquire()
                try:
                    f = open('caselist/%s.list' % path[1], 'w')
                    p = pickle.Pickler(f)
                    p.dump(caselist)
                    f.close()
                finally:
                    changed = True
                    clocks = False
                    lock.release()
                print '*' * 32
                print '+Caselist saved+'
                print 'Judge: %s' % path[1]
                print 'Client: %s' % self.client_address[0]
                print '*' * 32
                self.do_GET()
            else:
                pass
        else:
            pass

    def do_GET(self):
        """Respond to a GET request."""
        global changed
        global updated
        jvalues = readJudges()
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()
        top = """<html><head><meta http-equiv="content-type" content="text/html; charset=UTF-8"><title>%s</title></head><body><center><style type="text/css"><!-- a {color: blue; text-decoration: none;} a.a {color: red;} a:hover {color: green;} --></style>"""
        bottom = """</center></body></html>"""
        path = map(urllib.unquote, string.split(self.path, '/'))
        if path[1]:
            self.wfile.write(top % path[1])
            if path[1] == 'administrator':
                readSettings()
                changed = True
                self.wfile.write("""<h1>Адміністративний розділ ще не працездатний!</h1>""")
            elif path[1] in [a[0] for a in jvalues]:
                self.wfile.write("""<h1>%s</h1><br />""" % path[1])
                if not path[2]:
                    try:
                        f = open('caselist/%s.list' % path[1])
                        newfile = False
                    except IOError:
                        newfile = True
                    if newfile:
                        rvalues = readRooms()
                        self.wfile.write("""<form action="/%s/+" enctype="multipart/form-data" method="post">""" % path[1])
                        self.wfile.write("""Номер залу: <select name="room">""")
                        defaultroom = [item[1] for item in jvalues if (item[0] == path[1])][0]
                        for i in rvalues:
                            self.wfile.write("""<option%s>%s""" % (' selected' * (defaultroom == i), i))
                        self.wfile.write("""</select><br /><br />""")
                        self.wfile.write("""Файл зі списком справ: <input type="file" name="casefile" />&nbsp;""")
                        self.wfile.write("""<button type="submit">Завантажити</button></form>""")
                    else:
                        caselist = pickle.Unpickler(f).load()
                        f.close()
                        self.wfile.write("""<table border="1" cellpadding="4">""")
                        self.wfile.write("""<thead><tr><td>№ п/п</td><td>Суддя</td><td>№ залу</td><td>Час слухання</td><td>№ справи</td><td>Позивач</td><td>Відповідач</td></tr></thead><tbody>""")
                        for r in range(len(caselist)):
                            self.wfile.write("""<tr>""")
                            self.wfile.write("""<td>%s</td>""" % caselist[r][0])
                            self.wfile.write("""<td nowrap>%s</td>""" % caselist[r][1])
                            self.wfile.write("""<td>%s</td>""" % caselist[r][2])
                            self.wfile.write("""<td>%s</td>""" % caselist[r][3])
                            self.wfile.write("""<td>%s</td>""" % caselist[r][4])
                            self.wfile.write("""<td>%s</td>""" % caselist[r][5])
                            self.wfile.write("""<td>%s</td>""" % caselist[r][6])
                            self.wfile.write("""</tr>""")
                        self.wfile.write("""</tbody></table>""")
                        self.wfile.write("""<br /><a href="/%s/!">Видалити список справ</a><br />""" % path[1])
                else:
                    if path[2] == '!':
                        self.wfile.write("""<form action="/%s/-">""" % path[1])
                        self.wfile.write("""<i>Ви впевнені, що бажаєте видалити список справ?</i><br /><br />""")
                        self.wfile.write("""<button type="submit">Так, видалити</button></form>""")
                        self.wfile.write("""<a href="/%s/">Повернутися до списку справ</a><br />""" % path[1])
                    elif path[2] == '-':
                        lock.acquire()
                        try:
                            os.remove('caselist/%s.list' % path[1])
                        finally:
                            changed = True
                            lock.release()
                        self.wfile.write("""<a href="/%s/">Завантажити новий список справ</a><br />""" % path[1])
                        print '*' * 32
                        print '+Caselist removed+'
                        print 'Judge: %s' % path[1]
                        print 'Client: %s' % self.client_address[0]
                        print '*' * 32
                    else:
                        f = open('caselist/%s.list' % path[1])
                        caselist = pickle.Unpickler(f).load()
                        f.close()
                        for r in range(len(caselist)):
                            if caselist[r][5] == path[2]:
                                caselist[r][0] = True
                            else:
                                caselist[r][0] = False
                        lock.acquire()
                        try:
                            f = open('caselist/%s.list' % path[1], 'w')
                            p = pickle.Pickler(f)
                            p.dump(caselist)
                            f.close()
                        finally:
                            changed = True
                            clocks = False
                            lock.release()
                        self.wfile.write("""<table border="1" cellpadding="4">""")
                        self.wfile.write("""<thead><tr><td>№ п/п</td><td>Суддя</td><td>№ залу</td><td>Час слухання</td><td>№ справи</td><td>Позивач</td><td>Відповідач</td></tr></thead><tbody>""")
                        for r in range(len(caselist)):
                            self.wfile.write("""<tr>""")
                            self.wfile.write("""<td>%s</td>""" % caselist[r][0])
                            self.wfile.write("""<td nowrap>%s</td>""" % caselist[r][1])
                            self.wfile.write("""<td>%s</td>""" % caselist[r][2])
                            self.wfile.write("""<td>%s</td>""" % caselist[r][3])
                            self.wfile.write("""<td>%s</td>""" % caselist[r][4])
                            self.wfile.write("""<td>%s</td>""" % caselist[r][5])
                            self.wfile.write("""<td>%s</td>""" % caselist[r][6])
                            self.wfile.write("""</tr>""")
                        self.wfile.write("""</tbody></table>""")
                        self.wfile.write("""<br /><a href="/%s/-">Видалити список справ</a><br />""" % path[1])
            else:
                self.wfile.write("""<h1>Ви звернулися за помилковою адресою!</h1>""")
                print '*' * 32
                print '-Wrong request-'
                print 'Request: %s' % self.path
                print 'Client: %s' % self.client_address[0]
                print '*' * 32
            self.wfile.write("""<br /><a href="/">Повернутися до вибору суддів</a>""")
        else:
            self.wfile.write(top % ovalues[0])
            self.wfile.write("""<h1>Вибір судді</h1>""")
            for i in jvalues:
                try:
                    f = open('caselist/%s.list' % i[0])
                    f.close()
                    fileexist = True
                except:
                    fileexist = False
                self.wfile.write("""<a%s href="/%s/">%s</a><br />""" % (' class="a"' * fileexist, i[0], i[0]))
        self.wfile.write(bottom)

class WebInterface(threading.Thread):
    def run(self):
        global logfile
        server_class = BaseHTTPServer.HTTPServer
        self.httpd = server_class((ovalues[18], int(ovalues[19])), WIHandler)
        logfile = open('log/iboard_%s.log' % datetime.date.today(), 'a+', 0)
        sys.stdout = sys.stderr = logfile
        print time.asctime(), "Server Starts - %s:%s" % (ovalues[18], int(ovalues[19]))
        try:
            self.httpd.serve_forever()
        except KeyboardInterrupt:
            pass
    def stop(self):
        global logfile
        self.httpd.shutdown()
        self.httpd.server_close()
        print time.asctime(), "Server Stops - %s:%s" % (ovalues[18], int(ovalues[19]))
        logfile.close()

workw = window.width - window.width / int(ovalues[9]) - window.width / int(ovalues[10])
workh = window.height - window.height / int(ovalues[11])

updateRows()

clockback(None)

pyglet.clock.schedule_interval(callback, (float(ovalues[17]) / window.height))
pyglet.clock.schedule_interval(clockback, 1)

head = [u'№', u'Суддя', u'Зал', u'Час', u'№ справи', u'Позивач', u'Відповідач']
layhead = []
for cel in range(len(head)):
    document = pyglet.text.decode_attributed(("""%s%s%s""" % (ovalues[20], ovalues[1], head[cel])) % (workh / int(ovalues[16])))
    layhead.append(pyglet.text.layout.IncrementalTextLayout(document, workw, workh, multiline = True))
    layhead[cel].height = layhead[cel].content_height
    wlist[cel] = layhead[cel].width = layhead[cel].content_width + workw / 64
    layhead[cel].anchor_y = 'top'
    layhead[cel].y = workh

image = pyglet.image.load('image.jpg')
image.anchor_x = image.width / 2
image.anchor_y = image.height / 2

wi = WebInterface()
wi.start()

try:
    mainlogfile = open('log/iboard_display.log', 'a+', 0)
    sys.stdout = sys.stderr = mainlogfile
    pyglet.app.run()
    mainlogfile.close()
except:
    pass

wi.stop()
